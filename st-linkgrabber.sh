#!/bin/sh
regex='(((http|https|ftp|gopher)|mailto)[.:][^ >"\t]*|www\.[-a-z0-9.]+)[^ .,;\t>">\):]'
url=$(grep -Po "$regex" | uniq | st-fzfmenu -i -g 80x24+0+0) || exit
[ -z "$url" ] && exit 0
$BROWSER "$url"
