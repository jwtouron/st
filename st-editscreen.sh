#!/bin/sh
tmpfile1=$(mktemp /tmp/st-editscreen-1.XXXXXX)
tmpfile2=$(mktemp /tmp/st-editscreen-2.XXXXXX)
trap 'rm "$tmpfile1" "$tmpfile2"' 0 1 15

cat <<EOF > "$tmpfile1"
set rtp=
set clipboard=unnamed,unnamedplus
set ignorecase
set incsearch
set nomodifiable
set readonly
set smartcase

nnoremap Y y$
nnoremap <expr> q ":qa!\<cr>"

autocmd VimEnter * normal! G{}0
EOF

cat > "$tmpfile2"

st -i -g 160x48+0+0 -e vim -u "$tmpfile1" "$tmpfile2"
